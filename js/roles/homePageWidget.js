SiteVibes.trendingWidgetTabs =  //key value pairs for trending widget tabs.  
{                               //value is whats displayed, key is whats used to sort
    'tf=Just%20Added%20to%20Cart':'Just Added To Cart',
    'tf=Hot%20Now&vn=0':'Viewing Now',
    'pt=1':'Popular On Pinterest',
    'fb=1':'Popular On Facebook',
    'tf=Popular%20Today&vn=0':'Popular Today',
    'tf=Popular%20this%20Week&vn=0':'Popular this Week',
    'tags=Womens>Running': 'Womens Running'
};

SiteVibes.trendingWidgetAfter = '#someBanner';
SiteVibes.trendingWidgetDataStore = {}
SiteVibes.trendingWidgetMaxPage = {};
SiteVibes.assembleWidgetScaffolding = function(){
    
    var sizer = 5;

    if(window.innerWidth < 1380){
        sizer = 4;
    }

    if(window.innerWidth < 900){
        sizer = 3;
    }

    var ref = this;
    var tabsHTML = '';

    var widgetHTML = 
        '<div id="sv_trending_widget_wrap" class="sv_trending_widget_init sv_trending_widget_'+this.trendingWidgetSide+' size'+sizer+'">\
            <div id="sv_trending_expando_wrap">\
                <div id="sv_trending_expando">What\'s Trendingbbb?</div>\
            </div>\
            <div id="sv_trending_widget_tab_wrap">\
                <ul></ul>\
            </div>\
            <div id="sv_trending_widget_content_wrap">\
                <div id="sv_more_content_left" class="sv_trending_arrow_left size'+sizer+' ">\
                    <div class="arrow-left jmk"></div>\
                </div>\
                <div id="sv_trending_widget_content">\
                    <div id="sv_trending_widget_content_inner"></div>\
                </div>\
                <div id="sv_more_content_right" class="sv_trending_arrow_right size'+sizer+'">\
                    <div class="arrow-right"></div>\
                </div>\
            </div>\
        </div>';
        
    var injectAfter = document.querySelector(this.trendingWidgetAfter);

    if(injectAfter != null){
        injectAfter.insertAdjacentHTML('afterend', widgetHTML);
        if(!this.trendingWidgetLoadOpen && !this.trendingWidgetInitBounce){
            var wWrap = document.getElementById('sv_trending_widget_wrap');
            wWrap.style[this.trendingWidgetSide]='-'+wWrap.offsetWidth+'px';
        }
    }
    
    //add event listener for load more action
    document.getElementById('sv_more_content_right')
    .addEventListener('click',function(e){

        if(this.className.match(/sv_arrow_disabled/)
        ){
            return;  //dont fire request if more arrow was disabled 
        }

        ref.svCounter++;
        ref.renderSeenWidgetItems('+');
        return;

    });

    //add event listener for load less action
    document.getElementById('sv_more_content_left')
    .addEventListener('click', function(e){
        if(this.className.match(/sv_arrow_disabled/)
        ){
            return;  //dont fire request if more arrow was disabled 
        }

        ref.svCounter--;
        ref.renderSeenWidgetItems('-');
        return;
    });
};

    //create the widgets html as a string
SiteVibes.createWidgetHtmlString = function(items, dir){

//responsive hack
    var sizer = 5;

    if(window.innerWidth < 1380){
        sizer = 4;
    }

    if(window.innerWidth < 900){
        sizer = 3;
    }



    if(!dir){dir = '0';}

    var itemsHTML = '<div class="customWidgetWrap" style="left:'+dir+'%">';
    var prodCount=0;

    var shareHTML = '';
    if(this.injectSocialButtons){
        var shareHTML = '<div class="sv_share_wrap">';

        for(i=0;i<this.socialShareButtons.length;i++){
                if(typeof(this[this.socialShareButtons[i]+'Share']) == 'function'){
                    shareHTML += '<div onclick="SiteVibes.'+this.socialShareButtons[i]+'Share(this);return false;" \
                          class="sv_share sv_'+this.socialShareButtons[i]+'"></div>';
                }
        }
        shareHTML += '</div>';
    }

    for(i=0;i<items.length;i++){
       
        var prices = decodeURIComponent(items[i]._source.price);
        var was = decodeURIComponent(items[i]._source.price);
        var sale = '';
        var price_split = prices.split('|')
        if (price_split.length > 1) {
            was = price_split[0];
            sale = price_split[1];
        }
        prodCount++;


        //figure out if we should set current viewers
        var currentViewers = false;

        if ((items[i]._source.trendingTime == 'Hot Now' || 
             items[i]._source.trendingTime == 'Just Added to Cart') && 
             items[i]._source.unique_count > 2)
        {
            currentViewers = true;
        }
        
        //figure out trending type
        var tTypeObj = SiteVibes.determineTrendingType(items[i]._source);

        itemsHTML += 
            '<div class="deptitemprod dept-product">\
	<div class="dept-product-inner">\
		<div>\
			<i style="color:'+tTypeObj.color+'!important;" class="'+tTypeObj.iconClass+'"></i>'+
			shareHTML+
			'<p class="sv-trending-type">'+tTypeObj.tType+'</p><br>\
		</div>\
        <br>\
		<div class="dept-product-img" style="height:auto;">\
			<a href="'+items[i]._source.url+'">\
				<img src="'+items[i]._source.image+'" style="width:90%">\
			</a>\
            '+(currentViewers ? '<p  class="current-viewers">'+items[i]._source.unique_count+' people viewing now</p>' : '')+'\
		</div>\
		<div class="dept-product-lower">\
			<div class="ItemInfo">\
				<a class="dept-txt-title" href="'+items[i]._source.url+'">\
					'+items[i]._source.name+'\
				</a><br>\
				<div class="dept-txt-sku">Item # '+items[i]._source.description+'</div>\
				<div class="prices">\
					<span class="was">'+(sale != '' ? '<del>'+was+'</del>' : was)+'</span>\
					'+(sale != '' ? '<br><span class="sale">'+sale+'</span>' : '')+'\
				</div>\
			</div>\
		</div>\
	</div>\
</div>';

    }
    
    //if we have less than 3 inject some empty ones
    var toAdd = sizer - items.length;
    for(i=1;i<=toAdd;i++){
        itemsHTML += 
            '<div class="sv_trending_widget_content_item">\
                <div class="sv_thumb_wrap">\
                    <a href=""></a>\
                </div>\
                <div class="sv_desc_wrap">\
                    <span class="sv_name"><a href="" title=""></a></span>\
                    <span class="sv_price"></span>\
                    <span class="sv_sale_price"></span>\
                </div>\
            </div>';
    }

    itemsHTML += '</div>';
    return itemsHTML;
};

SiteVibes.trendingWidgetApiCall = function(){
    var ref = this;

    var category = '';
    category = Object.keys(this.trendingWidgetTabs).reduce(function(a,b){return a+encodeURIComponent(b)+','},'');
    
    //reset or initiate seen products array
    if(!this.twidgetSeen || this.svCounter == 1){
        this.twidgetSeen = ['empty'];  //this value will never be an actual ID
    }

    var url = this.apiUrl + "Multi/?_="+Date.now()+
            "&callback=SiteVibes.trendingWidgetjsonPHandler&o="+this.trendingWidgetHardLimit+"&fs=0&p="+
            this.svCounter.toString()+
            "&mode=widget&seen="+this.twidgetSeen.toString()+
            "&tags="+category;

    url += '&'+this.trendingWidgetFilter;
    
    var script = document.createElement('script');
    script.src = url;
    script.className = "sv_jsonp_script";
    document.getElementsByTagName('head')[0].appendChild(script);
    
},

SiteVibes.trendingWidgetjsonPHandler = function(data){
    var ref = this;
    //clear node
    var widgetWrap = document.getElementById('sv_trending_widget_content_inner');        
    //try to remove the appended script for a clean dom
    var node = document.querySelector('.sv_jsonp_script');
    if(node.parentNode){
        node.parentNode.removeChild(node);
    }

//responsive hack
    var sizer = 5;
    if(window.innerWidth < 1380){
        sizer = 4;
    }
    if(window.innerWidth < 900){
        sizer = 3;
    }

    var tabs = [];
    //first lets loop through and check for results
    //dont forget to chop off any modulus of page size from the results
    for(k in data){
        if(data.hasOwnProperty(k)){
            if(data[k].hits.hits.length >= sizer){
                tabs.push(this.trendingWidgetTabs[k]);
                var extra = data[k].hits.hits.length - (data[k].hits.hits.length%sizer);                
                this.trendingWidgetDataStore[this.trendingWidgetTabs[k]] = data[k].hits.hits.slice(0,extra);
                var fun = this.trendingWidgetDataStore[this.trendingWidgetTabs[k]].length/sizer;
                this.trendingWidgetMaxPage[this.trendingWidgetTabs[k]] = fun;
            }
        }
    }

    //next draw the tabs, and render the first tabs items
    var tabWrap = document.getElementById('sv_trending_widget_tab_wrap');
    tabWrap = tabWrap.getElementsByTagName('ul')[0];
    
    if(tabs.length > 0){
        for(i = 0;i<tabs.length;i++){
            var tab = '<li class="sv_trending_widget_tab '+(i==0?'active':'')+'"><span>'+tabs[i]+'</span></li>'    
            tabWrap.insertAdjacentHTML('beforeend', tab);
        };
        this.renderActiveTabItems();
    }

    //set tab listeners
    var tabs = document.getElementsByClassName('sv_trending_widget_tab');
    for(i = 0;i<tabs.length;i++){
        tabs[i].addEventListener('click', function(e){
            if(!this.className.match(/active/)){
                ref.svCounter = 1;
                //ref.trendingWidgetDataStore = []; //empty tracking array

                for(ii = 0;ii < tabs.length; ii++){
                    tabs[ii].className = 'sv_trending_widget_tab';
                }
                this.className += ' active';
                ref.renderActiveTabItems();
            }
        });
    }
    return;

}

SiteVibes.renderActiveTabItems = function(){

//responsive hack
    var sizer = 5;
    if(window.innerWidth < 1380){
        sizer = 4;
    }
    if(window.innerWidth < 900){
        sizer = 3;
    }

    //get active tab
    var widgetWrap = document.getElementById('sv_trending_widget_content_inner');
    var active = document.querySelector('.sv_trending_widget_tab.active');
    var more = document.getElementById('sv_more_content_right');
    var less = document.getElementById('sv_more_content_left');
    var outerWrap = document.getElementById('sv_trending_widget_wrap');

    //enable or disable the arrows
    if(this.trendingWidgetDataStore[active.textContent].length > sizer){
        more.className = more.className.replace(/sv_arrow_disabled/,'');
        less.className = less.className.replace(/sv_arrow_disabled/,'');
    }else{
        more.className = more.className+' sv_arrow_disabled';
        less.className = less.className+' sv_arrow_disabled';
    }
    
    //get and draw the item html
    var itemsHTML = '';
    itemsHTML = this.createWidgetHtmlString(this.trendingWidgetDataStore[active.textContent].slice(0,sizer));
    widgetWrap.innerHTML = '';
    widgetWrap.insertAdjacentHTML('afterbegin', itemsHTML);
    this.svCounter=1;

    outerWrap.className = outerWrap.className.replace(/\s*sv_trending_widget_init\s*/, '');
    this.widgetPaginating = 2;

};
//////////////////////////////////////////////////OLD STUFF BELOW

/*
        var itemsHTML = '';
        if(this.trendingWidgetDataStore.length > 0){
            itemsHTML = this.createWidgetHtmlString(this.trendingWidgetDataStore.slice(0,sizer));
            this.trendingWidgetMaxPage = Math.ceil(this.trendingWidgetDataStore.length/sizer);
        }
        
        widgetWrap.innerHTML = '';
        widgetWrap.insertAdjacentHTML('afterbegin', itemsHTML);

        //handle initialization stuff
        var outerWrap = document.getElementById('sv_trending_widget_wrap');

        if(this.trendingWidgetInit && this.trendingWidgetInitBounce){
            //set initial offset, outer wrap + slide open tab height
            var expando   = document.getElementById('sv_trending_expando_wrap');

            var off = 0;
            off = off - outerWrap.offsetWidth;
            off = off - expando.offsetHeight;

            outerWrap.style[this.trendingWidgetSide] = off+'px';
            outerWrap.className = outerWrap.className.replace(/\s*sv_trending_widget_init\s*///, '');
            
/*
            this.trendingWidgetInit = false;
            
        }else if(this.trendingWidgetInit){
            
        }
        this.widgetPaginating = 2;
    };*/

    SiteVibes.renderSeenWidgetItems = function(direction){

        var sizer = 5;
        if(window.innerWidth < 1380){
            sizer = 4;
        }
        if(window.innerWidth < 900){
            sizer = 3;
        }

        var active = document.querySelector('.sv_trending_widget_tab.active');
        //make sure were not in the middle of scrolling the widget already
        if(this.widgetPaginating != 2){
            return;
        }

        if(this.svCounter > this.trendingWidgetMaxPage[active.textContent]){
            this.svCounter = 1;
        }
        if(this.svCounter < 1){
            this.svCounter = this.trendingWidgetMaxPage[active.textContent];
        }

        var widgetWrap = document.getElementById('sv_trending_widget_content_inner');

        var baseIndex = (this.svCounter - 1) * sizer;

        var itemsHTML = this.createWidgetHtmlString(this.trendingWidgetDataStore[active.textContent].slice(baseIndex, (baseIndex+sizer)), direction+'100');
        if(direction == '+' && this.widgetPaginating == 2){
            widgetWrap.insertAdjacentHTML('beforeend', itemsHTML);
            var els = document.querySelectorAll('.customWidgetWrap');
            this.widgetPaginating=0;
            for(i=0;i<els.length;i++){
                this.slideWidgetUp(els[i]);
            }
        }
        else if(direction == '-' && this.widgetPaginating == 2){
            widgetWrap.insertAdjacentHTML('afterbegin', itemsHTML);
            var els = document.querySelectorAll('.customWidgetWrap');
            this.widgetPaginating=0;
            for(i=0;i<els.length;i++){
                this.slideWidgetDown(els[i]);
            }
        }
        
    }

    SiteVibes.slideWidgetUp = function(el){
        //container - 300px margin top, remove first 3 items, reset margin top
        var ref = this;
        
        var iteration = 0;
        var totalIterations = 60;

        var startValue = el.style.left.replace(/[^\d\.-]/g, '');
        startValue = (startValue == '' ? 0 : startValue);
        var changeInValue = -100;
        var end = (+startValue + changeInValue);

        var tick = function(){
            var h = ref.easeInOutQuad(iteration++, +startValue, changeInValue, totalIterations);
            el.style.left = h+'%';

            if(h != end){
               (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
            }
            else{
                if(h != 0){
                    el.parentNode.removeChild(el);
                }
                
                ref.widgetPaginating++;
            }
        };
        tick();

    }

    SiteVibes.slideWidgetDown = function(el){
        var ref = this;
        
        var iteration = 0;
        var totalIterations = 60;

        var startValue = el.style.left.replace(/[^\d\.-]/g, '');
        startValue = (startValue == '' ? 0 : startValue);
        var changeInValue = 100;
        var end = (+startValue + changeInValue);

        var tick = function(){
            var h = ref.easeInOutQuad(iteration++, +startValue, changeInValue, totalIterations);
            el.style.left = h+'%';
            if(h != end){
               (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
            }
            else{
                if(h != 0){
                    el.parentNode.removeChild(el);
                }
                ref.widgetPaginating++;
            }
        };
        tick();

        
    };

SiteVibes.prepareTWPing = function(node){
        var parent = node.parentNode.parentNode.parentNode;
        var session = this.uuid();
        var today = new Date();
        var fdate = today.getUTCFullYear()+"-"+
                   (today.getUTCMonth()+1)+"-"+
                    today.getUTCDate()+" "+today.getUTCHours()+":"+
                    today.getUTCMinutes()+":"+today.getUTCSeconds();
        this.q.session = session;
        this.q.time = fdate;

        var media = parent.querySelector('.dept-product-img > a > img');
        media = (media && media.src ? media.src : '');
        this.q.image = media;

        var text  = parent.querySelector('.dept-txt-title');
        text = (text && text.textContent ? text.textContent : '');
        this.q.name = text;

        this.q.url             = parent.querySelector('a').href;
        this.q.host            = encodeURIComponent(location.host);

        this.q.productCategory = this.productCategory;
        this.q.pagetype        = 'home_page_widget';
        this.q.id              = encodeURIComponent(this.tmiID);
    
        this.q.price = encodeURIComponent(parent.querySelector('.prices > span').textContent.trim());


        this.ping();
};
